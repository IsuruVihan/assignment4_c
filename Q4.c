/*
 * Isuru Harischandra
 * 202032
 *
 * */

// This program takes temperature in Celsius and return the temperature in Fahrenheit

#include <stdio.h>

int main() {

    float temp_c, temp_f;
    int condition = 1;

    // getting user inputs
    do {
        printf("Enter the temperature in Celsius :");
        scanf("%f", &temp_c);
        if (temp_c < -273.15) { // if the user input is invalid
            printf("Invalid input! Temperature should be greater than or equal to -273.15.\n");
        } else {
            condition = 0; // if the user input is valid
        }
    }
    while (condition);

    // calculating temperature in Fahrenheit
    temp_f = (temp_c * 9 / 5) + 32;

    // print temperature in Fahrenheit
    printf("Temperature in Fahrenheit = %.2f F", temp_f);

    return 0;

}
