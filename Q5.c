/*
 * Isuru Harischandra
 * 202032
 *
 * */

// This program outputs values according to &, ^, ~, <<, >> operators

#include <stdio.h>

int main() {

    int A = 10, B = 15;

    // Bitwise AND (&)
    printf("Bitwise AND (&) : A & B = %d\n", A & B);
    // Bitwise eXclusive OR / Bitwise XOR (^)
    printf("Bitwise XOR (^) : A ^ B = %d\n", A ^ B);
    // Bitwise One's complement / Bitwise NOT (~)
    printf("Bitwise NOT : ~ A = %d\n", ~A);
    // Shift left
    printf("Shift left : A << 3 = %d\n", A << 3);
    // Shift right
    printf("Shift right : B >> 3 = %d\n", B >> 3);

    return 0;

}