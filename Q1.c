/*
 * Isuru Harischandra
 * 202032
 *
 * */

// This program takes three integers as user inputs and calculate sum & average of them

#include <stdio.h>

int main() {

    int int1, int2, int3, sum;
    float average;

    // getting user inputs
    printf("Enter your first integer :");
    scanf("%d", &int1);
    printf("Enter your second integer :");
    scanf("%d", &int2);
    printf("Enter your third integer :");
    scanf("%d", &int3);

    // calculating sum and average
    sum = int1 + int2 + int3;
    average = ((float)int1 + (float)int2 + (float)int3) / (float)3;

    // print results
    printf("Sum of %d, %d and %d is : %d\n", int1, int2, int3, sum);
    printf("Average of %d, %d and %d is : %.2f", int1, int2, int3, average);

    return 0;

}
