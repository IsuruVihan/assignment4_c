/*
 * Isuru Harischandra
 * 202032
 *
 * */

// This program takes two integers as user inputs and swap them without using a third variable

#include <stdio.h>

int main() {

    int int1, int2;

    // getting user inputs
    printf("Enter your first integer :");
    scanf("%d", &int1);
    printf("Enter your second integer :");
    scanf("%d", &int2);

    // before swapping
    printf("Before swapping : num1 = %d, num2 = %d\n", int1, int2);

    // swapping
    int1 = int1 + int2;
    int2 = int1 - int2;
    int1 = int1 - int2;

    // printing results
    printf("After swapping : num1 = %d, num2 = %d\n", int1, int2);

    return 0;

}
