/*
 * Isuru Harischandra
 * 202032
 *
 * */

// This program takes radius and height of a cone as user input and calculate the volume of the cone

#include <stdio.h>

# define PI 3.14159265359

int main() {

    float radius, height, volume;
    int condition = 1;

    // get height
    do {
        printf("Enter the height of the cone (cm):");
        scanf("%f", &height);
        if (height <= 0) { // if the user input is invalid
            printf("Invalid input! Height should be greater than 0.\n");
        } else {
            condition = 0; // if the user input is valid
        }
    }
    while (condition);

    condition = 1;

    // get radius
    do {
        printf("Enter the radius of the cone (cm):");
        scanf("%f", &radius);
        if (radius <= 0) { // if the user input is invalid
            printf("Invalid input! Radius should be greater than 0.\n");
        } else {
            condition = 0; // if the user input is valid
        }
    }
    while (condition);

    // calculate volume
    volume = PI * radius * radius * height / 3;

    // print volume
    printf("Volume of the cone is = %.3f cm^3", volume);

    return 0;

}
